from fastapi import FastAPI

import scraper

app = FastAPI()


@app.get("/")
async def root():
    return {"message": scraper.get_msg()}
